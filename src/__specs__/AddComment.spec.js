import React from 'react';
import { mount } from 'enzyme';

import Article from '../Article.jsx';

describe('Add a comment to an article', () => {
    let article, input, textarea;

    let props = {
        title: 'SpaceX going to mars!',
        content: 'Stuff about going to mars'
    };

    beforeEach(() => {
        article = mount(<Article title={props.title} content={props.content} />);
        input = article.find('input');
        textarea = article.find('textarea');
    });

    it('Should update username on change', () => {
        let expectedUsername = 'Test Person';
        input.simulate('change', { target: { value: expectedUsername }});
        expect(article.state('username')).to.equal(expectedUsername);
    });

    it('Should update content on change', () => {
        let expectedContent = 'Testing content';
        textarea.simulate('change', { target: { value: expectedContent }});
        expect(article.state('content')).to.equal(expectedContent);
    });

    it('Should add comment on submit', (done) => {
        setTimeout(() => {
            let button = article.find('button');

            input.simulate('change', { target: { value: 'Tom'}});
            textarea.simulate('change', { target: { value: 'content'}});

            button.simulate('click');

            expect(article.state('comments')).to.have.length(4);

            done();
        }, 100);
    });
});
