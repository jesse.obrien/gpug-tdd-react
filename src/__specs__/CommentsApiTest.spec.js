import React, { Component } from 'react';
import { mount } from 'enzyme';
import Article from '../Article.jsx';
import Comment from '../Comment.jsx';
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter';

describe('Test Article Receives Comments from API', () => {
    let mock = new MockAdapter(axios);
    let mockCommentsList = {
        data: {
            comments: [
                { id: 1, username: 'Tom Bombadil', content: 'I went on an adventure like this once' },
                { id: 2, username: 'Jeff Bezos', content: 'SpaceX sucks.' },
                { id: 3, username: 'Saul Goodman', content: '$5 for client confidentiality is worth it' }
            ]
        }
    };

    mock.onGet('/comments').reply(200, mockCommentsList);

    let article;

    let props = {
        title: 'SpaceX going to mars!',
        content: 'Stuff about going to mars'
    };

    beforeEach(() => {
        article = mount(<Article title={props.title} content={props.content} />);
    });

    it('Should render all comments from API', (done) => {
        setTimeout(() => {
            expect(article.state('comments')).to.equal(mockCommentsList.data.comments);

            done();
        }, 100);
    });

    it('Should render comment contents from API', (done) => {
        setTimeout(() => {
            let theFirstComment = article.find(Comment).first();

            expect(theFirstComment.type()).to.equal(Comment);
            expect(theFirstComment.hasClass('comment'));

            expect(theFirstComment.find('h3').text()).to.equal(mockCommentsList.data.comments[0].username);
            expect(theFirstComment.find('p').text()).to.equal(mockCommentsList.data.comments[0].content);

            done();
        }, 100);
    });
});

