import React, { Component } from 'react';
import { shallow } from 'enzyme';

import Article from '../Article.jsx';

describe('Render Article and Comments', () => {
    let article;

    let props = {
        title: 'SpaceX going to mars!',
        content: 'Stuff about going to mars'
    };

    beforeEach(() => {
        article = shallow(<Article title={props.title} content={props.content} />);
    });

    it('Should render an Article', () => {
        let instance = article.instance();

        expect(instance).to.be.instanceOf(Article);

        expect(instance.props.title).to.equal(props.title);
        expect(instance.props.content).to.equal(props.content);

        expect(article.find('h1')).to.have.length(1);
        expect(article.find('h1').contains(props.title));

        expect(article.find('p')).to.have.length(1);
        expect(article.find('p').contains(props.content));
    });
});
