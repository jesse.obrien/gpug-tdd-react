import React, { Component, PropTypes } from 'react'

export default class Comment extends Component {
    render() {
        return (
            <div className="comment">
                <h3>{this.props.username}</h3>
                <p>{this.props.content}</p>
            </div>
        );
    }
}

Comment.defaultProps = {
    username: null,
    content: null
};

Comment.propTypes = {
    username: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired
};
