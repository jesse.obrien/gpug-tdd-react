import React, { Component, PropTypes } from 'react';
import axios from 'axios';

import Comment from './Comment.jsx';

export default class Article extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: undefined,
            content: undefined,
            comments: []
        };

        this.handleChangeUsername = this.handleChangeUsername.bind(this);
        this.handleChangeContent = this.handleChangeContent.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        axios.get('/comments')
            .then(response => response.data.data)
            .then(data => {
                this.setState({
                    comments: data.comments
                });
            });
    }

    handleChangeUsername(event) {
        this.setState({
            username: event.target.value
        });
    }

    handleChangeContent(event) {
        this.setState({
            content: event.target.value
        });
    }

    handleSubmit() {
        let newComments = this.state.comments;

        newComments.push({
            id: 4,
            username: this.state.username,
            content: this.state.content
        });

        this.setState({
            comments: newComments
        });
    }

    render() {
        return (
            <section>
                <h1>{this.props.title}</h1>
                <p>{this.props.content}</p>

                <div className="comment-box">
                    <input type="text" name="username" onChange={this.handleChangeUsername} />
                    <textarea name="content" onChange={this.handleChangeContent}></textarea>
                    <button onClick={this.handleSubmit}>Post Comment</button>
                </div>
                <div className="comment-list">
                    {this.state.comments.map(comment => {
                        return <Comment key={comment.id} username={comment.username} content={comment.content} />
                    })}
                </div>
            </section>
        );
    }
}

Article.defaultProps = {
    title: null,
    content: null,
    comments: []
};

Article.propTypes = {
    title: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    comments: PropTypes.array
};
